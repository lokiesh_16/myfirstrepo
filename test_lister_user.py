import pytest
import requests
import json


@pytest.mark.parametrize("userid, firstname", [(1, "George")])
def test_list_valid_user(supply_url, userid, firstname):
    url = supply_url + "/users/" + str(userid)
    resp = requests.get(url)
    json_response = resp.json()
    assert resp.status_code == 200
    assert json_response['data']['id'] == userid
    assert json_response['data']['first_name'] == firstname


def test_list_invalid_user(supply_url):
    url = supply_url + "/users/50"
    resp = requests.get(url)
    assert resp.status_code == 404




