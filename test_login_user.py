import pytest
import requests
import json


def test_create_user(supply_url):
    url = supply_url + "/users"
    payload = {
        "name": "lokesh",
        "job": "testere"
    }
    resp = requests.post(url, data=payload)
    json_response = resp.json()
    assert resp.status_code == 201, resp.text
    print(json.dumps(resp.json(), indent=4))

def test_valid_login(supply_url):
    url = supply_url + "/login/"
    data = {'email':"test@gmail.com", 'password':'something'}
    resp = requests.post(url, data=data)
    json_response = resp.json()
    assert resp.status_code == 200, 'login credentials are wrong if status code is not equal to 200'


