import pytest
import requests
import json

#unknown resource
def test_user_by_id(supply_url):
    url = supply_url + "/users?page=2"
    resp = requests.get(url)
    json_response = resp.json()

    assert json_response['page'] == 2
    assert json_response["data"][0]["id"] == 7

def test_user_by_lastname(supply_url):
    url = supply_url + "/users?page=2"
    resp = requests.get(url)
    json_response = resp.json()

    assert json_response["data"][0]["last_name"] != "lokesh"